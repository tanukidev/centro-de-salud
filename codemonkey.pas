unit codemonkey;

{$codepage utf-8}{$H+}

interface

uses
  Classes, SysUtils, crt,unit_arbol,archivo,tiposdedatos, DateUtils;
type
  opcionC=byte;


procedure ABMC_sintomas(var archS:T_ARCHIVO_SINTOMAS;var y:T_sintomas; pos:word);
procedure ABMC_personas(var archP:T_ARCHIVO;var archS:T_ARCHIVO_SINTOMAS;var arbolitoP:T_punt );
function convertirStrAbool(s:string):boolean;
function convertBoolAstr(s:boolean):string;
PROCEDURE MOSTRAR(VAR x:T_sintomas);
procedure mostrar_tabla_datos_personales(encabezado:boolean;x:T_dato;y:T_sintomas;posy:byte);
function buscarID(var y:t_archivo_sintomas;id:word):word;
procedure agregar_elemento_menu (var incy,incx:byte; texto:string);
procedure escribir (texto:string;var entrada:string; var posx,posy:byte);
procedure dibujar_menu();
implementation

Function ascii(mask: pchar): integer; cdecl; varargs; external 'msvcrt.dll' name 'printf';
function convertirStrAbool(s:string):boolean;
  begin
  if ((upcase(s))=('S')) or ((upcase(s)) =('SI')) then convertirStrAbool:=true else convertirStrAbool:=false
  end;
function convertBoolAstr(s:boolean):string;
  begin
  if s=true then convertBOolaStr:='verdadero' else convertboolastr:='negativo' ;
  end;

 {
             PROCEDURE INICIALIZAR(VAR V:T_VECTOR);
 VAR
 I:1..N;
 BEGIN
 TITULO ('****** OPCIÓN DE INICIALIZAR ******');
 FOR I:= 1 TO N DO
 BEGIN
 sintomas[I].dolorgarganta:='';
 sintomas[I].fiebre:='';
 sintomas[I].perdidagusto:='';
 sintomas[I].perdidaolfato:='';
 sintomas[I].confirmadocovid:='';
 sintomas[I].tos:'';
 sintomas[I].dolorcabeza:='';
 END;
 end;       }


PROCEDURE MOSTRAR(VAR x:T_sintomas);
 VAR
 dia:1..14;
 BEGIN
 dia:=1;
 writeln('****** OPCIÓN DE MOSTRAR ******');
 while x.t_vector[dia].ingresado <> false do
 BEGIN
 textcolor(green);
 writeln('-----------------------------------------');

 writeln('       ========= dia ',dia,'========');
 textcolor(dia);
 writeln('dolor de cabeza: ', convertBoolAstr(x.t_vector[dia].dolorcabeza));
 writeln('perdida del olfato: ', convertBoolAstr(x.t_vector[dia].perdidaolfato));
 writeln('perdida del gusto: ', convertBoolAstr(x.t_vector[dia].perdidagusto));
 writeln('dolor de garganta: ', convertBoolAstr(x.t_vector[dia].dolorgarganta));
 writeln('tos: ', convertBoolAstr(x.t_vector[dia].tos));
 writeln('fiebre: ', convertBoolAstr(x.t_vector[dia].fiebre));
 writeln('covid positivo: ', convertBoolAstr(x.confirmadocovid));
 inc(dia,1);
 writeln('presione para continuar');
 readkey;
 END;
 end;
procedure mostrar_tabla_datos_personales(encabezado:boolean;x:T_dato;y:T_sintomas;posy:byte);
type
  campos=(ID,AP,NOM,CI,DI,DNI,TEL);
  dato=array[campos] of integer ;
 var
 DATOS:dato;
 campo:campos ;
begin
   DATOS[ID]:=0;
  DATOS[ap]:=19  ;
  DATOS[nom]:=DATOS[ap]+20;
 DATOS[ci]:=DATOS[nom]+15;
  DATOS[di]:=DATOS[ci]+15;
  DATOS[dni]:=DATOS[di]+9;
  DATOS[tel]:=DATOS[dni]+15;

if encabezado =true then begin
     textcolor(lightgreen);
     gotoxy(0,0);
      write('numero de consulta ');
      gotoxy(DATOS[ap],0);
      write(' | Apellido ');
      gotoxy(DATOS[nom],0);
      write(' | Nombres ');
      gotoxy(DATOS[ci],0);
      write(' | ciudad ' );
      gotoxy(DATOS[di],0);
      write(' | direccion ');
      gotoxy(DATOS[dni],0);
      write(' | DNI ');
      gotoxy(DATOS[tel],0);
      write(' | Teléfono ' );
    end;

          gotoxy(DATOS[ID],posy);
          write(x.id);
          gotoxy(DATOS[ap],posy);
          write(x.Apellido);
          gotoxy(DATOS[nom],posy);
          write(x.Nombre);
          gotoxy(DATOS[ci],posy);
          write(x.ciudad);
          gotoxy(DATOS[di],posy);
          write(x.direccion);
          gotoxy(DATOS[dni],posy);
          write(x.dni);
          gotoxy(DATOS[tel],posy);
          write(x.TEL);
      end;
  {procedure convertirfecha(fecha1,fecha2:t_fecha;var fechaint1,fechaint2:integer);

     begin
          dia1+(mes1 * 100)+(anno1 * 10000

    end;   }
procedure ABMC_sintomas(var archS:T_ARCHIVO_SINTOMAS;var y:T_sintomas; pos:word);
var


dia,i:byte;
diadiff:word;
auxdia:word;
sintoma:string[2];
control:char;
begin
clrscr;
dia:=1 ;

textBackground(7) ;
 clrscr;
 seek(archS,filesize(archS)-1);
 write(archS,y);
 if ((y.FECHAhoy.anno) and (y.FECHAhoy.mes) and (y.FECHAhoy.dia))= 0 then begin
                                                                            decodeDate(date,y.FECHAhoy.anno,y.FECHAhoy.mes,y.FECHAhoy.dia);
                                                                            end;

  ///       textcolor(lightblue)    ;
//         writeln('datos de la persona. Nombre: ',x.Nombre,' numero consulta: ',x.id);
         textcolor(white);


                if (DaysBetween(encodedate(y.FECHAhoy.anno,y.FECHAhoy.mes,y.FECHAhoy.dia),Date))> 14 then
                  writeln(' Ya se han ingresado todos los datos del dia')
                     else
                       begin
                        dia:=DaysBetween(encodedate(y.FECHAhoy.anno,y.FECHAhoy.mes,y.FECHAhoy.dia),Date);
                        gotoxy(20,5);
                        writeln(' CONTESTE PRESIONANDO s PARA SI Y n PARA NO ');
                        gotoxy(5,5);
                        writeln(' tiene dolor de cabeza? ');
                        sintoma:=readkey;
                        y.t_vector[dia].dolorcabeza:=convertirStrAbool(sintoma);

                        writeln(' tiene falta de olfato?');
                        sintoma:=readkey;
                        y.t_vector[dia].perdidaolfato:=convertirStrAbool(sintoma);

                        writeln(' tiene falta de gusto?');
                        sintoma:=readkey;
                        y.t_vector[dia].perdidagusto:=convertirStrAbool(sintoma);

                        writeln(' tiene dolor de garganta?');
                        sintoma:=readkey;
                        y.t_vector[dia].dolorgarganta:=convertirStrAbool(sintoma);

                        writeln(' tiene tos?');
                        sintoma:=readkey;
                        y.t_vector[dia].tos:=convertirStrAbool(sintoma);

                        writeln(' tiene fiebre?');
                        sintoma:=readkey;
                        y.t_vector[dia].fiebre:=convertirStrAbool(sintoma);



                        writeln(' ha dado positivo en covid-19?');
                        sintoma:=readkey;
                        y.confirmadocovid:=convertirStrAbool(sintoma);



                        y.t_vector[dia].ingresado:=true;
                        write(archs,y);


    end;
 end;
function buscarID(var y:t_archivo_sintomas;id:word):word;    //devuelve la posicion del archivo que contiene la misma id
 var i:integer;
     reg:T_sintomas;
begin
   i:=0;

    while (not (eof(y)) )and (reg.id<>id) do
    begin
         seek(y,i);
         read(y,reg);

         if reg.id=id then buscarID:=i else buscarID:=0 ;
         inc(i,1);
    end;
end;

// procedure ingresarDatos(x:t_dato;y:t_sintoma);
procedure ABMC_personas(var archP:T_ARCHIVO;var archS:T_ARCHIVO_SINTOMAS;var arbolitoP:T_punt );


var
 x:t_dato    ;
 y:t_sintomas;
 tecla:char;
 entrada:string[9];
 letra:char;
 input:boolean;
 pos:word;
 entero:integer;
 auxArb:T_punt;
 SegEntryExist:boolean ;
 opcion,opcion1y,opcion2y,opcion3y:byte;

 enter,exit:boolean;
 incx,incy:byte;
 procedure ingresarDatosPersona(var x:t_dato; var arch:t_archivo);  //pide datos que se guardan en T_dato
           var xaux:t_dato;
           begin

        gotoxy(50,1);
        clrscr;
        writeln('ingrese datos :');


        x.id:=(filesize(archp));
      //  writeln('Fecha de hoy (dia de la consulta)');

        writeln('Apellido');
        readln(entrada);
        x.Apellido:=entrada;
         writeln('Nombres');
         readln(entrada);
         x.Nombre:=entrada;



         writeln('direccion');
         readln(entrada);
         x.direccion:=entrada;

         writeln('ciudad');
         readln(entrada);
         x.ciudad:=entrada;

         writeln('Número de documento');
         readln(entrada);
         x.DNI:=entrada;



         writeln('Fecha nacimiento  ');
         write('dia: ');
         readln(entero);
         x.FECHANAC.dia:=entero;
         write('  mes  ');
         readln(entero);
         x.FECHANAC.mes:=entero;
         write('  año  ');
         readln(entero);
         x.FECHANAC.anno:=entero;

         writeln('Teléfono');
         readln(entrada);
         x.TEL:=entrada;
         guardar(x,arch);



 end;


begin

                   //1 alta, 2 modificacion, 3 consulta
             //alta o alta masiva de el archivo de personas y el de sintomas opcionalmente
    enter:=false;
    opcion:=1;
    clrscr;
    dibujar_menu();
    INCY:=ESIMY;
    INCX:=ESIMX;
    agregar_elemento_menu(incy,incx,'ALTA DE DATOS ');
    agregar_elemento_menu(incy,incx,'MODIFICACION DE DATOS ');
    agregar_elemento_menu(incy,incx,'CONSULTA DE DATOS ');




              incy:=2  ;
                  while (enter <>true )or(exit<> true) do begin
                     TECLA:=READKEY;
                      if tecla=#00 then                           //para leer la tecla de las flechitas y elegir la opción correspondiente
                        begin
                          tecla:=readkey;

                          case tecla of

                            #72:if opcion>=1 then opcion:=opcion-1;
                            #80:if opcion<=3 then opcion:=opcion+1;
                            end;
                            case opcion of
                                 1:begin
                                   gotoxy(esimx+2, esimy+incy);
                                   ascii('%c',254);
                                   //inc(incy,incy);
                                    end;
                                 2: begin
                                    gotoxy(esimx+2, esimy+incy+2);
                                    ascii('%c',254);
                                 end;
                                 3: begin
                                    gotoxy(esimx+2, esimy+incy+opcion);
                                    ascii('%c',254);
                           end;
                      end;
                end
                    else if tecla=#13 then enter:=true                    //tecla enter precionada
                                   else if tecla = #27 then exit:=true;                //tecla escape presionada
             end ;


            if enter=true then begin
               case opcion of
                 1: begin
                   clrscr;
                   ingresarDatosPersona(x,archP);                     //ingresa los datos de la persona en el archivo de persona (nombre, id, dni, etc)
                   writeln('presione una tecla para continuar');
                   readkey;
                   clrscr;
                   ABMC_sintomas(archS,y,filepos(archP));
                   writeln('presione una tecla para continuar');     //sintomas de la persona de covid
                   readkey;
                   clrscr;
                 end;
                 2:begin

                   end;
                 3: begin

                    end;

                 end;
          end;
          {

           while input=true do
            begin

              if input = true then  begin
                                    clrscr;
                                    gotoxy(50,5);

                                    writeln('1:ir al siguiente paciente ');
                                    writeln('2:ingresar sintoma del siguiente dia');

                                    readln(letra);
                                    if letra = '1' then  begin

                                                         ingresarDatos(X);
                                                         write(archP,x);
                                                         clrscr; gotoxy(50,5);
                                                         {writeln('ingresa sintomas? S/n');
                                                         readln(letra);
                                                         input:=convertirStrAbool(letra);
                                                         if input =true then begin
                                                                        ABMC_sintomas(archS,y);

                                                                        end   }
                                                         end
                                    else if letra = '2' then begin
                                                               ABMC_sintomas(archS,y);
                                                              end ;

                                     end;
                  clrscr;
                  gotoxy(50,5);
                writeln('ingresar datos ? S/n');
                readln(letra);
                input:=convertirStrAbool(letra);

           end;
         end;

         2: begin
          pos:=1;
                 clrscr;
                gotoxy(5,5);
                textcolor(cyan);
                writeln('ingrese DNI de la persona de la cual quiere obtener los datos');
                readln(entrada);
                busqueda_inorden(arbolitoP,entrada,pos);

                {writeln(pos);
                writeln(filesize(archP)); }
                seek(archP,pos);
                 read(archP,x);
                if buscarID(archS,x.ID)  <> 0 then seek(archS,buscarID(archS,x.ID));

               { {$i-}
                read(archS,y);
                if ioresult<>0 then SegEntryExist:=false else SegEntryExist:=true ;
                {$i+} }
                textcolor(lightred);
                writeln();
                writeln('---------DATOS ACTUALES----------');
                  writeln('numero de consulta ',x.ID);
                  textcolor(lightgreen);
                  writeln('Apellido ',x.apellido );
                   writeln('Nombres ',x.Nombre);

                   writeln('direccion ',x.direccion);
                   writeln('Número de documento ',x.DNI);
                   writeln('Teléfono ',x.TEL );
                   writeln('-----------------');

                   {if SegEntryExist = true then} mostrar(y);

                   Textcolor(red);
                   WRITELN('AVISO: MODIFICAR IMPLICA REESCRIBIR TODOS LOS DATOS DE LA PERSONA y de los sintomas');
                   WRITELN('MODIFICAR? ? S/n');
                    read(tecla);
                    TEXTCOLOR(WHITE);
                    if upcase(tecla)='S' then
                                          begin
                                          ingresarDatos(x);
                                          write(archP,x);
                                          ABMC_sintomas(archS,y);
                                          end;
                end; }
                clrscr;
       end;

procedure agregar_elemento_menu (var incy,incx:byte; texto:string);

                  begin
                     // gotoxy(esimx+incx,esimy+incy);
                              gotoxy(incx,incy);
                      ascii('%c',254);
                      gotoxy(wherex+1,wherey);
                      write(' ',texto);
                      INC(incy,incy);
                  end;
 procedure escribir (texto:string;var entrada:string; var posx,posy:byte);

                  begin
                      gotoxy(posx,posy);
                      writeln(texto);
                      readln(entrada);
                      inc(posy,1);
                      end;
  procedure dibujar_menu();

 var
                   VU,HL,VD,HR:byte;          //Vertical Up, horizontal Lrft, vertical Down, Horizontal Right


                   begin


                                          //lineas verticales superiores
                     for VU:=esimx to esdmx do
                        begin
                             gotoxy(VU,esimy);
                             ascii('%c', 205);     //═

                         end;
                         gotoxy(esimx,esimy);
                         write(#201);
                     for HL:=esimy to eiimy do           //lineas horizontales izquierda
                        begin
                             gotoxy(esimx,HL);
                              ascii('%c', 186);    //║
                         end;


                   for VD:=eiimx to eidmx do               //lineas verticales inferiores
                     begin
                          gotoxy(VD,eiimy);
                           ascii('%c', 205);
                          end;


                   for hr:=EIDMy downto ESDMy do               //lineas verticales derecha
                     begin
                          gotoxy(eidmx,hr);
                          ascii('%c', 186);                    //║
                          end;
                  gotoxy(eiimx,eiimy);                   //╚
                  ascii('%c', 200);       { ╚ }
                  gotoxy(eidmx,eidmy);
                  ascii('%c', 188);                             //╝
                  GotoXY(esimx,esimy);
                  ascii('%c', 201);       { ╔ }
                  GotoXY(esdmx,esdmy);
                  ascii('%c', 187);       { ╗ }
 end;

  {if (dia1+(mes1 * 100)+(anno1 * 10000))<=((raiz_sint^.INFO.seguimiento[1].fecha.ano *1 0000)+(raiz_sint^.INFO.seguimiento[1].fecha.mes * 100)+raiz_sint^.INFO.seguimiento[1].fecha.dia) then
    begin
          if (dia2+(mes2 * 100)+(anio2*10000))>=((raiz_sint^.INFO.seguimiento[1].fecha.ano * 10000)+(raiz_sint^.INFO.seguimiento[1].fecha.mes*100)+raiz_sint^.INFO.seguimiento[1].fecha.dia) then
          contador:=contador+1; }

end.

