unit UNIT_ARBOL;
INTERFACE

USES CRT,tiposdedatos,manejo_arbol;
type

T_DATO_arbol= record
               CLAVE:string[40];
               pos_arch:word;

end;

T_PUNT = ^T_NODO;

T_NODO = RECORD
           SAI,SAD: T_PUNT ;
           INFO:T_DATO_ARBOL;
         END;

 PROCEDURE CREAR_ARBOL (VAR RAIZ:T_PUNT);
 PROCEDURE AGREGAR (VAR RAIZ:T_PUNT; X:T_DATO_arbol);
 FUNCTION ARBOL_VACIO (RAIZ:T_PUNT): BOOLEAN;
 FUNCTION ARBOL_LLENO (RAIZ:T_PUNT): BOOLEAN;
 procedure busqueda_inorden (var RAIZ:T_PUNT; X:string;var output:word);
 PROCEDURE inorden (raiz:T_PUNT;opcion:byte;var arch:T_archivo;var arch2:T_ARCHIVO_SINTOMAS);

implementation
 PROCEDURE inorden (raiz:T_PUNT;opcion:byte;var arch:T_archivo;var arch2:T_ARCHIVO_SINTOMAS);

 begin

       if raiz <>nil then begin
                        inorden(RAIZ^.SAI,opcion,arch,arch2);

                        listado_arbol(raiz^.info.pos_arch,opcion,arch,arch2);

                        inorden(RAIZ^.SAD,opcion,arch,arch2);
                     end;

      end;

procedure busqueda_inorden (var RAIZ:T_PUNT; X:string;var output:word);     //devuelve el valor de lo que estás buscando y se detiene ni bien lo encuentra

begin
     if RAIZ^.INFO.CLAVE = X then output:=RAIZ^.INFO.pos_arch
        else
           if (RAIZ^.INFO.CLAVE > X )and (RAIZ^.SAI <> nil) then
                               begin


                                    busqueda_inorden(RAIZ^.SAI,X,output);

                               end
            ELSE IF (RAIZ^.INFO.CLAVE < X )and (RAIZ^.SAD <> nil) then
                                      BEGIN

                                         busqueda_inorden(RAIZ^.SAD,X,output);
                                      END


     end;

PROCEDURE CREAR_ARBOL (VAR RAIZ:T_PUNT);
BEGIN
    RAIZ:= NIL;
  END;
 PROCEDURE AGREGAR (VAR RAIZ:T_PUNT; X:T_DATO_arbol);
  BEGIN
        IF RAIZ = NIL THEN
                            BEGIN
                            NEW (RAIZ);
                            RAIZ^.INFO:= X;
                            RAIZ^.SAI:= NIL;
                            RAIZ^.SAD:= NIL;
                            END
                    ELSE   IF RAIZ^.INFO.clave > X.clave THEN AGREGAR (RAIZ^.SAI,X)
                                             ELSE AGREGAR (RAIZ^.SAD,X)

   END;

 FUNCTION ARBOL_VACIO (RAIZ:T_PUNT): BOOLEAN;
   BEGIN
        ARBOL_VACIO:= RAIZ  = NIL;
   END;

 FUNCTION ARBOL_LLENO (RAIZ:T_PUNT): BOOLEAN;
    BEGIN
         ARBOL_LLENO:= GETHEAPSTATUS.TOTALFREE < SIZEOF (T_NODO);
    END;



 END.

