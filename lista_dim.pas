unit lista_dim;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, tiposdedatos;
const n=40;
      m=130;
type

      T_PUNT_L = ^T_NODO;
      T_NODO = RECORD
            INFO: record
                   CLAVE:string[40];
                   pos_arch:word;
               end;
            SIG:T_PUNT_L;

      END;
      T_LISTA = RECORD
              CAB,ACT: T_PUNT_L;
              TAM: 1..N;
              pos:byte;
      END;
var
  L:T_lista;
       X:T_dato;
PROCEDURE CREARLISTA(VAR L:T_LISTA);
PROCEDURE AGREGAR (VAR L:T_LISTA; X:T_DATO);
FUNCTION TAMANIO (VAR L:T_LISTA): BYTE;
FUNCTION LISTA_LLENA (VAR L:T_LISTA): BOOLEAN;
FUNCTION LISTA_VACIA (VAR L:T_LISTA): BOOLEAN;
PROCEDURE ELIMINARLISTA (VAR L:T_LISTA;BUSCADO: STRING; VAR X:T_DATO);
PROCEDURE SIGUIENTE(VAR L:T_LISTA);
PROCEDURE PRIMERO (VAR L:T_LISTA);
FUNCTION FIN (L:T_LISTA): BOOLEAN;
PROCEDURE RECUPERAR (L:T_LISTA; VAR E:T_DATO);

implementation

  PROCEDURE CREARLISTA(VAR L:T_LISTA);
  BEGIN
    L.TAM:=0;
    L.CAB:=NIL;
  END;
 // PROCEDURE AGREGAR (VAR L:T_LISTA; X:T_DATO);
  VAR DIR, ANT, ACT: T_PUNT_L;
    BEGIN
      NEW (DIR);
      DIR^.INFO:= X;
    IF (L.CAB= NIL) OR (L.CAB^.INFO.Nombre > X.NOMbre) THEN
      BEGIN
        DIR^.SIG:= L.CAB;
        L. CAB:=DIR;
      END

      ELSE
      BEGIN
        ANT:= L.CAB;
        ACT:= L.CAB^.SIG;
        WHILE (ACT <> NIL) AND (ACT^.INFO.NOMbre < X.NOMbre) DO
          BEGIN
            ANT:= ACT;

            ACT:= ACT^.SIG
          END;
      DIR^.SIG:= ACT;
      ANT^.SIG:= DIR;
      END;
      INC(L.TAM)
  END.
  PROCEDURE SIGUIENTE(VAR L:T_LISTA);
  BEGIN
  L.ACT:= L.ACT^.SIG;
  inc(l.pos,1);
  END;
  PROCEDURE PRIMERO (VAR L:T_LISTA);
  BEGIN
  L.ACT:= L.CAB;
  l.pos:=1;
  END;
  FUNCTION FIN (L:T_LISTA): BOOLEAN;
  BEGIN
  FIN:=L.ACT = NIL;
  END;
  FUNCTION TAMANIO (VAR L:T_LISTA): BYTE;
  BEGIN
  TAMANIO:= L.TAM;
  end;
  FUNCTION LISTA_LLENA (VAR L:T_LISTA): BOOLEAN;
  BEGIN
  LISTA_LLENA:= GETHEAPSTATUS.TotalFree < SIZEOF(T_DATO) ;
  END;
  FUNCTION LISTA_VACIA (VAR L:T_LISTA): BOOLEAN;
  BEGIN
  LISTA_VACIA:= L.TAM=0;
  END;
  PROCEDURE RECUPERAR (L:T_LISTA; VAR E:T_DATO);
  BEGIN
  E:= L.ACT^.INFO;
  END;

 // PROCEDURE ELIMINARLISTA (VAR L:T_LISTA;BUSCADO: STRING; VAR X:T_DATO);
  VAR ACT, ANT: T_PUNT_L;
  BEGIN
  IF (L.CAB^.INFO.NOMbre= BUSCADO) THEN

  BEGIN
  X:= L.CAB^.INFO;
  ACT:=L.CAB;
  L.CAB:=L.CAB^.SIG;
  DISPOSE (ACT);
  END

  ELSE
  BEGIN
  ANT:=L.CAB;
  ACT:= L.CAB^.SIG;
  WHILE (ACT <> NIL) AND (ACT^.INFO.NOMbre< X.Nombre) DO
  BEGIN
  ANT:=ACT;
  ACT:= ACT^.SIG;
  END;
  X:= ACT^.INFO;

  ANT^.SIG:= ACT^.SIG;
  DISPOSE (ACT);
  END ;
  DEC(L.TAM);
  end;
  end.


end.
