unit MENU_LIMPIO_V3;

{$mode objfpc}{$H+}  {$codepage utf-8}

interface

uses
  Classes,crt,tiposdedatos,archivo, unit_arbol,codemonkey,manejo_arbol,ESTADISTICAS,sysutils;
const
  n=20 ;
  m=30 ;
  l=12;

  ancho=50   ;
  alto =25   ;
  correcionx = 40  ;
  correxxiony= 2  ;



  ESIMx= correcionx   ;                     //esquina superior izquierda menu   ╔
  ESIMy= correxxiony ;

  ESDMx= correcionx+ancho ;                         //'        '        derecha menu ╗
  ESDMy= correxxiony;

  EIIMx =correcionx  ;                         //esquina inferior izquierda menu  ╚
  EIIMy =alto+correxxiony ;

  EIDMx =correcionx+ancho;                         //'        '        DERECHA MENU    ╝
  EIDMy=alto+correxxiony;





VAR  //GLOBALES  PRUEBA
       OPCION:byte;

       archP:T_archivo ;
       archS:T_archivo_Sintomas;

       arbolitoP:T_punt;
       arbolitoN:T_punt;

      PROCEDURE   OPCION2_MOSTRAR_DATOS_PACIENTE_POR_FECHA ();
      PROCEDURE   OPCION3_LISTADO_POR_FECHAS_DE_CONSULTA();
     PROCEDURE    CASOS_CONFIRMADOS ();
      PROCEDURE   MOSTRAR_LISTADO_FECHA_O_NOMBRE_MENU_PRINCIPAL ();
      PROCEDURE   BUSQUEDA_POR_FECHA()                         ;
      procedure   PORCENTAJES_MENU_PRINCIPAL ()  ;
       procedure atras (var arch:t_archivo);

       //NECESARIO MENU
      procedure   agregar_elemento_menu (var incy,incx:byte; texto:string);
      procedure escribir (texto:string;var entrada:string; var posx,posy:byte);
       procedure  ColorearOpcion(opcion_actual,opcion:byte);
       Procedure PantallaPrincipal(var opcion:byte);
       PROCEDURE SELECION(opcion:byte);
      procedure dibujar_menu();
implementation
 Function ascii(mask: pchar): integer; cdecl; varargs; external 'msvcrt.dll' name 'printf';
 procedure dibujar_menu();

 var
                   VU,HL,VD,HR:byte;          //Vertical Up, horizontal Lrft, vertical Down, Horizontal Right


                   begin


                                          //lineas verticales superiores
                     for VU:=esimx to esdmx do
                        begin
                             gotoxy(VU,esimy);
                             ascii('%c', 205);     //═

                         end;
                         gotoxy(esimx,esimy);
                         write(#201);
                     for HL:=esimy to eiimy do           //lineas horizontales izquierda
                        begin
                             gotoxy(esimx,HL);
                              ascii('%c', 186);    //║
                         end;


                   for VD:=eiimx to eidmx do               //lineas verticales inferiores
                     begin
                          gotoxy(VD,eiimy);
                           ascii('%c', 205);
                          end;


                   for hr:=EIDMy downto ESDMy do               //lineas verticales derecha
                     begin
                          gotoxy(eidmx,hr);
                          ascii('%c', 186);                    //║
                          end;
                  gotoxy(eiimx,eiimy);                   //╚
                  ascii('%c', 200);       { ╚ }
                  gotoxy(eidmx,eidmy);
                  ascii('%c', 188);                             //╝
                  GotoXY(esimx,esimy);
                  ascii('%c', 201);       { ╔ }
                  GotoXY(esdmx,esdmy);
                  ascii('%c', 187);       { ╗ }
 end;
procedure ColorearOpcion(opcion_actual,opcion:byte);
   begin
      if opcion=opcion_actual then TextBackground(blue) else TextBackground (black);
   end;


   Procedure PantallaPrincipal(var opcion:byte);
   var
     exit:boolean;
     tecla:char;
posxem,posyem: byte;
   begin


     opcion:=1;
     exit:=false;
     while not exit do
     begin
     posxem:=2  ;
     posyem:=2;

      dibujar_menu();
      ColorearOpcion(opcion,1);
       agregar_elemento_menu(posxem,posyem,('1.ALTA DE DATOS'));
      ColorearOpcion(opcion,2);
       agregar_elemento_menu(posxem,posyem,('2.MODIFICACION DE DATOS'));
      ColorearOpcion(opcion,3);
       agregar_elemento_menu(posxem,posyem,('3.CONSULTA'));
      ColorearOpcion(opcion,4);
       agregar_elemento_menu(posxem,posyem,('4.ESTADISTICAS'));
      ColorearOpcion(opcion,5);
       agregar_elemento_menu(posxem,posyem,('5.SALIR'));

      tecla:=readkey;
      if tecla=#00 then
      begin
        tecla:=readkey;
      end;
       case tecla of
       #72:if opcion>1 then opcion:=opcion-1;
       #80:if opcion<5 then opcion:=opcion+1;
       #13:exit:=true;
       end;
         if opcion <1 then opcion:=5;   //para subir op_1 a op_5
         if opcion >5 then opcion:=1;   //para bajar op_5 a op_1
    end;
     CLRSCR;
     SELECION(opcion);

  end;
   PROCEDURE SELECION (opcion:byte);
     var
     archivoPersona:T_archivo;
       archivoSintoma:T_archivo_Sintomas;
      raizNom:T_punt  ;
      raizDni:T_punt;
      claveDNI,claveNOM:T_DATO_ARBOL ;
      i:word;
      x:t_dato;
     BEGIN
       crear(archivoSintoma);
       crear(archivoPersona);
       CREAR_ARBOl(raizNom);
       CREAR_ARBOl(raizDni);
                                                      //cargar arbol de nombres y apellido
       for i:=0 to (filesize(archivoPersona)-1) do
         begin
          seek(archivoPersona,i);
          read(archivoPersona,x);
          claveNOM.CLAVE:=x.apellido+' '+x.nombre;
          claveNOM.pos_arch:=i;        //le asigno la pocision del archivo donde está la clave NOMBRE
           AGREGAR(raiznom,clavenom);
           claveDNI.CLAVE:=x.DNI;
           claveDNI.pos_arch:=i;       //le asigno la pocision del archivo donde está la clave DNI
           AGREGAR(raizDNI,claveDNI);
         end;

       agregar(raizNOM,claveNOM);agregar(raizDNI,claveDNI);

     CASE OPCION OF
     1:ABMC_personas(archivoPErsona,archivoSintoma,arbolitoP);
     2:OPCION2_MOSTRAR_DATOS_PACIENTE_POR_FECHA (); //modificacion de datos??   ;
     3:OPCION3_LISTADO_POR_FECHAS_DE_CONSULTA();
     4:PORCENTAJES_MENU_PRINCIPAL () ;
     5:exit;
        END;
 end;

procedure agregar_elemento_menu (var incy,incx:byte; texto:string);

                  begin
                      gotoxy(esimx+incx,esimy+incy);
                      ascii('%c',254);
                      gotoxy(wherex+1,wherey+1);
                      write(' ',texto);
                      INC(incy,incy);
                  end;
 procedure escribir (texto:string;var entrada:string; var posx,posy:byte);

                  begin
                      gotoxy(posx,posy);
                      writeln(texto);
                      readln(entrada);
                      inc(posy,1);
                      end;
  PROCEDURE OPCION2_MOSTRAR_DATOS_PACIENTE_POR_FECHA ();
  var
        archivo_persona:T_archivo;
        archivo_sintoma:T_ARCHIVO_SINTOMAS;
         fin:boolean;
         x:t_dato;
         y:t_sintomas;
          pos:pos_Arch;
  BEGIN
       gotoxy(50,1);


          // while not(EOF(archP))DO
         while fin <> false DO
           BEGIN


             LISTADO(archivo_persona,pos,x,fin);
             LISTADO(archivo_sintoma,pos,y,fin);
             textcolor(lightred);
              write('numero de consulta ',x.ID);
              textcolor(lightgreen);
              write(' | Apellido ');
              write(' | Nombres ');
              write(' | ciudad ' );
              write(' | direccion ');
              write(' | Número de documento ');
              write(' | Teléfono ' );

              write('diagnostico covid: ', convertBoolAstr(y.confirmadocovid));


            writeln('presione una tecla para continuar');
         readkey;
          clrscr ;
  end;
          readkey;
end;
    PROCEDURE   MOSTRAR_LISTADO_FECHA_O_NOMBRE_MENU_PRINCIPAL ();
    var
            buscado:word;
BEGIN
       clrscr;
        writeln('mostrar listado ordenado por fecha(1) o por nombre(2)?');
        readln(buscado);
        case buscado of
                 1:  begin
                     writeln('listado ordenado por NOmbre confirmados positivos');
                      inorden(arbolitoP,2,archP,archS);
                      readkey;
                      clrscr;
                 end;
                   2:begin
                   writeln('listado ordenado por nombre');
                   inorden(arbolitoN,1,archP,archS) ;
                   readkey;
                   clrscr;
             end;  end;


END;

PROCEDURE   OPCION3_LISTADO_POR_FECHAS_DE_CONSULTA();
VAR
      fecha:t_fecha;
        pos:pos_Arch;
      enc:boolean;
     entero,dia_ingreso:integer;
          dia:t_fecha;
           Y:T_SINTOMAS;
           x:T_DATO;
BEGIN
         writeln ('ingrese la fecha de la cual desea ver las consultas:');
        enc:=false ;
         readln(dia_ingreso);
         fecha.dia:=entero;
         write('-');
         readln(entero);
         fecha.mes:=entero;
         write('-');
         readln(entero);
         fecha.anno:=entero;

         pos:=0;
         while (enc=false) and not(eof(archs))do

               begin
             seek(archs,pos);
             read(archs,y);
             inc(pos,1);

            If (fecha.dia=y.FECHAhoy.dia )and( fecha.mes=y.FECHAhoy.mes) and (fecha.anno=y.FECHAhoy.anno) Then
               begin
                  enc:=true ;
                  mostrar_todo_T_dato(x);
               end;
          end;
         clrscr;
        END;
PROCEDURE  CASOS_CONFIRMADOS ();
VAR
            Y:T_SINTOMAS;
        I:INTEGER;
        X:T_DATO;
BEGIN
  CLRSCR;
           GOTOXY(5,5);
        writeln(' A CONTINUACION SE PRESENTAN LOS CASOS CONFIRMADOS');
    FOR i:=0 TO FILESIZE(ARCHP)-1 DO
       BEGIN
        // LEE_REGISTRO (ARCH,I,REG); //SEEK(ARCH,POS); READ(ARCH, REG);
          seek(archS,I);
          seek(archP,I);
          textcolor(red);
          if posExist(archS,i) = true then read(archS,y) else writeln('no hay entrada de sintomas para esta persona');
          textcolor(lightblue);
          read(archP,x);
          mostrar_todo_T_dato(x);
          writeln(' presione para continuar') ;
          readkey;
          end;//mostrar nombre de los que dieron positivo
    clrscr;
END;

  PROCEDURE PORCENTAJES_MENU_PRINCIPAL ();
  var
           arch:t_archivo;
          opcion_PORCENTAJE:integer;
  BEGIN
    // repeat
           WRITELN('PORCENTAJES');
           WRITELN();
           WRITELN(' 1 porcentaje consultas diarias');
           WRITELN(' 2 porcentaje consultas vecinas');
           WRITELN(' 3 porcentaje menores de 25');
           WRITELN(' 4 PORCENTAJE DE CONFIRMADOS POR COVID');
           WRITELN(' 5 ATRAS');
           writeln();
           write('opcion :');read(opcion_PORCENTAJE);
           CASE OPCION_porcentaje OF
           1:BEGIN
             clrscr;
               porcentaje_consultas_diarias(archP);
              END;
           2:BEGIN
             clrscr;
               porcentaje_consultas_vecinos(archP);

              END;

              3:BEGIN
            clrscr;
               porcentaje_menores25(archP);

           END;
           4:BEGIN
               clrscr;
               //porcentaje_confirmados_covid(archS);
               readkey;
              END;
           5:BEGIN

              atras ( arch);


             END;

           end;//END CASE
   //  until (opcion_PORCENTAJE=5);
  end;
  procedure atras (var arch:t_archivo );

          begin
          clrscr;
        //  reset(arch);
           PantallaPrincipal(opcion);
          end;

  PROCEDURE BUSQUEDA_POR_FECHA()  ;
  var
          fechain,fechafin:t_fecha;
BEGIN
         clrscr;
                gotoxy(20,5)  ;
                writeln('ingrese desde que fecha buscar') ;
                writeln('ingrese dia');
                readln(fechain.dia);
                writeln('ingrese mes');
                readln(fechain.mes);
                writeln('ingrese año');
                readln(fechain.anno);


                consultas_entre_fechas(convertT_fechaAint(fechain),convertT_fechaAint(fechafin),archS);
end;
end.

