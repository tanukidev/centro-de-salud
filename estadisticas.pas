unit estadisticas;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,codemonkey,tiposdedatos,crt;


procedure porcentaje_consultas_vecinos(var arch:t_archivo);   //
procedure cant_confirmados_covid(var arch:t_archivo_sintomas);      //
procedure porcentaje_confirmados_covid(var arch:T_ARCHIVO_SINTOMAS);         //
procedure porcentaje_consultas_diarias(var arch:t_archivo);    //
procedure porcentaje_menores25 (var arch:t_archivo);             //
procedure consultas_entre_fechas(fechain,fechafin:integer;var archS:T_ARCHIVO_SINTOMAS);
 function convertT_fechaAint(fecha:t_fecha):integer;
implementation
function convertT_fechaAint(fecha:t_fecha):integer;
begin
   convertT_fechaAint:=(fecha.dia)+(fecha.mes*100)+(fecha.anno*1000);
end;

procedure porcentaje_consultas_vecinos(var arch:t_archivo);
var contador_vecinos,contador_total:real;
    porcentaje_vecino:real;
    reg:t_dato;
    i:word;
    begin
       contador_vecinos:=0;
       contador_total:=0;
       i:=0;
       seek(arch,0);
       while not eof (arch) do
       begin

          read (arch,reg);
          contador_total:=contador_total+1;
          if (upcase(reg.Ciudad) <> ('CONCEPCION DEL URUGUAY')) then
          contador_vecinos:=contador_vecinos+1;
          inc(i,1);
          seek(arch,i);
        end;
        porcentaje_vecino:=((contador_vecinos*100)/(contador_total));
        writeln('Casos vecinos: ',trunc(porcentaje_vecino),' %');
        readkey;
        end;
procedure cant_confirmados_covid(var arch:t_archivo_sintomas);
var cont_positivo:0..44000000;
    cont_negativo:0..44000000;
    reg:t_sintomas ;
     contador_total:0..44000000;
     str:string[2];
     i:word;
    begin
    cont_positivo:=0;
    cont_negativo:=0;
    i:=0;
       seek(arch,0);
    while not eof (arch) do
       begin
        read (arch,reg);
        contador_total:=contador_total+1;
If (convertBoolAstr(reg.confirmadocovid))=('si') Then
        cont_positivo:=cont_positivo+1;
        writeln( 'la cantidad de confirmados positivos de covid son', cont_positivo);
         str:=convertBoolAStr(reg.confirmadocovid);
        If str=('no')then
        cont_negativo:=cont_negativo+1;
        writeln ('la cantidad de no confirmados de covid son', cont_negativo);
        inc(i,1);
          seek(arch,i);
end;
 end;
procedure porcentaje_confirmados_covid(var arch:T_ARCHIVO_SINTOMAS);
var cont_positivo:real;
    cont_negativo:real;
    cont_total:real;
    porc_negativo:real;
    porc_positivo,cien:real;
    reg:t_sintomas;
    i:word     ;
    begin
    cont_positivo:=0;
    cont_negativo:=0;
    seek(arch,0);
    cien:=100;
    seek(arch,0);
    i:=0;
    while not eof (arch) do
       begin
        read (arch,reg);
        cont_total:=cont_negativo+cont_positivo;
        porc_negativo:=(cont_negativo*cien)/cont_total;
        writeln ('el porcentaje de confirmados positivos de covid son', porc_positivo);
        porc_positivo:=(cont_positivo*cien)/cont_total  ;
        writeln ('el porcentaje de no confirmados de covid son', porc_positivo);
        inc(i,1);
          seek(arch,i);
end;
  end;
procedure porcentaje_consultas_diarias(var arch:t_archivo);
   var  reg:T_sintomas;
        fecha_inicial,fecha_final,diferencia_dias,contador_consultas:integer;
        porcentaje_consultas_dias:real;
        i:word;
   begin
   i:=0;
   seek(arch,0);
      while not eof (arch) do
      begin
      contador_consultas:=1;
        if reg.ID=1 then
        fecha_inicial:=reg.fechahoy.dia+(reg.fechahoy.mes*30)+(reg.fechahoy.anno*365)
        else
        begin
        contador_consultas:=contador_consultas+1;
        fecha_final:=reg.fechahoy.dia+(reg.fechahoy.mes*30)+(reg.fechahoy.anno*365);
        end;
        inc(i,1);
          seek(arch,i);
      end;
      diferencia_dias:=fecha_final-fecha_inicial;
      porcentaje_consultas_dias:=(contador_consultas/diferencia_dias);
      writeln('Promedio de consultas diarias: ', porcentaje_consultas_dias,' %');
      readkey;
      end;

procedure porcentaje_menores25 (var arch:t_archivo);
  var contador_menores25,contador_ctotal:0..44000000;
    porcentaje_menores25:real;
    reg:T_sintomas;
     contador_consultas:word;
     i:word;
    begin
       i:=0;
       seek(arch,0);
        while not eof (arch) do
      begin
      contador_menores25:=0;
      contador_ctotal:=0;
      contador_consultas:=1;
      if reg.ID=1 then
      contador_consultas:=contador_consultas+0
      else contador_consultas:=contador_consultas+1;

      if (reg.fechahoy.anno)<(1996) Then
      contador_menores25:=contador_menores25+1;
      porcentaje_menores25:=(contador_menores25/contador_consultas);
      writeln('Promedio de consultas diarias: ', porcentaje_menores25,' %');
      readkey;
      inc(i,1);
          seek(arch,i);
      end;
      end;
procedure consultas_entre_fechas(fechain,fechafin:integer;var archS:T_ARCHIVO_SINTOMAS);
var
  pos:pos_arch;
  x:t_dato       ;
    y:t_sintomas;
    cont:word;
    i:word;
begin
   cont:=0;
   i:=0;
       seek(archS,0);
   while not eof (archS) do
         begin

              seek(archS,pos);

              read(archS,y);

              if (convertT_fechaAint(y.fechahoy)>fechain )and (convertT_fechaAint(y.fechahoy)<fechafin) then inc(cont,1);
           inc(i,1);
          seek(archS,i);
         end;
         writeln('la cantidad de consultas entre esas fechas es: ',cont);
   end;
end.
