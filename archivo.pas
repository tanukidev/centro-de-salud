unit Archivo;

{$mode objfpc}{$H+}

interface
 uses crt, SysUtils, tiposdedatos,unit_Arbol;
                                                                              //para crear el archivo de personas
        procedure Crear (var arch:t_archivo);                                     //para crear el archivo de sintomas
        procedure Crear(var arch:T_ARCHIVO_SINTOMAS);                            //para posicionar el puntero del archivo en la ultima ubicación sin errores
        procedure PosFinalArchivo(var arch: t_archivo);
        procedure PosFinalArchivo(var arch: t_archivo_sintomas);

        procedure Guardar(var L:t_dato; var arch: t_archivo);                       //para cuargar un dato en la posición final del archivo sin errores
        procedure Guardar (var L:t_sintomas; var arch: t_archivo_sintomas);
        PROCEDURE LEE_REGISTRO(VAR ARCH:T_ARCHIVO; POS:CARDINAL; VAR REG:t_dato);        //para leer los datos del archivo
        PROCEDURE LEE_REGISTRO(VAR ARCH:T_ARCHIVO_SINTOMAS; POS:CARDINAL; VAR REG:t_SINTOMAS);
        function posExist(var arch:T_ARCHIVO_SINTOMAS;i:word):boolean;
        function posExist(var arch:T_ARCHIVO;i:word):boolean;
        PROCEDURE LISTADO(VAR ARCH:T_ARCHIVO;pos:cardinal; var reg:t_dato;var FIN:BOOLEAN);
        PROCEDURE LISTADO(VAR ARCH:T_ARCHIVO_sintomas;pos:cardinal; var reg:t_sintomas;var FIN:BOOLEAN);

//   procedure SintCrearSint(var arch:T_ARCHIVO_SINTOMAS);

implementation
   PROCEDURE LEE_REGISTRO(VAR ARCH:T_ARCHIVO; POS:CARDINAL; VAR REG:t_dato);     //va a devolver el registro del archivo en la posición indicada en pos
BEGIN
    if posExist(arch,pos) = true then
                                  begin
                                    SEEK(ARCH, POS);
                                    READ(ARCH, REG);
                                    end;

end;

  PROCEDURE LEE_REGISTRO(VAR ARCH:T_ARCHIVO_SINTOMAS; POS:CARDINAL; VAR REG:t_SINTOMAS);
BEGIN
    if posExist(arch,pos) = true then
                                      begin
                                        SEEK(ARCH, POS);
                                        READ(ARCH, REG);
                                        end;
END;

procedure modificar_archivo(var arch:t_archivo; pos:cardinal; var reg:T_dato;var exito:boolean);    //modificar el registro en la posicion dada del archivo

begin
     seek(arch,pos);
     if not eof(arch) then
                     begin
                          write(arch,reg);
                          exito:=true;                               //exito es la variable que indica si la operación se pudo realizar (la posicion del archivo existe) o no se pudo realizar (la posición del archivo existe)
                     end
     else exito:=false;

end;

  procedure Crear(var arch:t_archivo);                                    //para crear el archivo de personas
  var reg:t_dato;
   begin
     AssignFile(arch,nomArchPersonas);
      if not FileExists(nomArchPersonas) then begin
                                                Rewrite(arch);
                                                seek(arch,0);
                                                reg.id:=0;
                                                write(arch,reg);  //establece la primera vez que se cree el archivo el ID en 1

                                                reg.apellido:='';
                                               reg.nombre:='';
                                               reg.DNI:='';
                                               reg.ID:=0;
                                               reg.ciudad:='';
                                               reg.direccion:='';
                                               reg.TEL:='';
                                               write(arch,reg);
                                                end
                                                else reset(arch);
   end;
  procedure Crear(var arch:T_ARCHIVO_SINTOMAS);                                 //para crear el archivo de sintomas
   var reg:t_sintomas;
   begin
     AssignFile(arch,nomArchSintomas);
      if not FileExists(nomArchSintomas) then begin
                                                Rewrite(arch);
                                                seek(arch,0);
                                                reg.id:=0 ;
                                                write(arch,reg);                           //inicializar el archivo con información vacía

                                                reg.confirmadocovid:=false;
                                                reg.FECHAhoy.anno:=0; reg.FECHAhoy.mes:=0;reg.fechahoy.dia:=0;
                                                end
                                                else reset(arch);
   end;
  procedure PosFinalArchivo(var arch: t_archivo);

     begin
        (Seek(arch,FileSize(arch)-1 ));

     end;
  procedure PosFinalArchivo(var arch: t_archivo_sintomas);

      begin
         (Seek(arch,FileSize(arch)-1 ));

      end;
  function posExist(var arch:T_ARCHIVO_SINTOMAS;i:word):boolean;  //funcion para determinar si la posición en el archivo de síntomas existte antes de un seek.
  begin

                  seek(arch,i);
                  if EOF(arch) = true then begin
                                  posExist:=false
                                  end
                  else posExist:=true;
                  end;


  function posExist(var arch:T_ARCHIVO;i:word):boolean;  //funcion para determinar si la posición en el archivo de síntomas existte antes de un seek.
 begin

                  seek(arch,i);
                  if EOF(arch) = true then begin
                                  posExist:=false
                                  end
                  else posExist:=true;
                  end;


procedure Guardar(var L:t_dato; var arch: t_archivo);

     begin
       PosFinalArchivo(arch);
       Write(arch,L);
     end;
procedure Guardar(var L:t_sintomas; var arch: t_archivo_sintomas);

     begin
       PosFinalArchivo(arch);
       Write(arch,L);
     end;


//procedure leerArchivo (var arch: T_archivo);


  PROCEDURE LISTADO(VAR ARCH:T_ARCHIVO;pos:cardinal; var reg:t_dato;var FIN:BOOLEAN);    //recupera la lista en la posicion dada

    BEGIN
      fin:=false  ;                                       //fin variable de control que es falsa si no es el fin de archivo y verdadera si lo es. Para usar en un while que compare por el fin

     SEEK(ARCH,POS);
                                                                                                                              //[1]t_dato [2]
     if NOT(EOF(ARCH)) then
                     READ(ARCH, REG)ELSE                       //reset: abre un archivo y si no existe tira un error; rewrite: sobreescribe/crea el archivo   IOresult
                                fin:=true;                      //close: para "cerrar un archivo"
                                                               //seek: ir a la pos x del archivo; read/write: leer/ecribir sobre archivo; sizeofile:el tamaño logico del archivo
   end;

PROCEDURE LISTADO(VAR ARCH:T_ARCHIVO_sintomas;pos:cardinal; var reg:t_sintomas;var FIN:BOOLEAN);    //recupera la lista en la posicion dada

    BEGIN
      fin:=false ;                                        //fin variable de control que es falsa si no es el fin de archivo y verdadera si lo es. Para usar en un while que compare por el fin
     SEEK(ARCH,POS);
     if NOT(EOF(ARCH)) then
                     READ(ARCH, REG)ELSE
                                   fin:=true;

   end;





  //--------------------------------------------------


   { procedure SintLISTADOsint(VAR ARCH:T_ARCHIVO_SINTOMAS);
    VAR
     REG:t_dato;
    BEGIN
     RESET(ARCH);
     WHILE NOT(EOF(ARCH)) DO
         BEGIN
           READ(ARCH, REG);
          { if reg.ESTADO = TRUE then
           MUESTRA_REGISTRO(REG);
           END;}

        end;
   end;            }


 {  procedure cargararbolDNI (VAR ARCH:T_ARCHIVO; var arbolito:T_punt);
        VAR
         REG:t_dato_Arbol;
         x:T_dato;
        I:CARDINAL;
        clavestr:string;

        BEGIN

            {$i-}
              seek(arch,1);
              read(arch,x);
            {$i+}
            if ioresult<>0 then

                            begin

                                 x.apellido:='';
                                 x.nombre:='';
                                 x.DNI:='';
                                 x.ID:=0;
                                 x.ciudad:='';
                                 x.direccion:='';
                                 x.TEL:='';
                                 write(arch,x);
                               END;
            FOR I:= 0 TO FILESIZE(ARCH)-1 DO
                               BEGIN
                                // LEE_REGISTRO (ARCH,I,REG); //SEEK(ARCH,POS); READ(ARCH, REG);
                                  seek(arch,I);
                                  read(arch,x);
                                  reg.CLAVE:=x.Dni;
                                  reg.pos_arch:=i;

                                 //MUESTRA_REGISTRO(REG);
                                 agregar(arbolito,reg);

                             END;

            end;  }


{   procedure cargararbolnom (VAR ARCH:T_ARCHIVO; var arbolito:T_punt);
        VAR
         REG:t_dato_Arbol;
         x:T_dato;
        I:CARDINAL;
        clavestr:string;
        BEGIN
              {$i-}
              seek(arch,1);
              read(arch,x);
            {$i+}
            if ioresult<>0 then

                            begin

                                 x.apellido:='';
                                 x.nombre:='';
                                 x.DNI:='';
                                 x.ID:=0;
                                 x.ciudad:='';
                                 x.direccion:='';
                                 x.TEL:='';
                                 write(arch,x);
                               END;

         FOR I:= 0 TO FILESIZE(ARCH)-1 DO
           BEGIN
            // LEE_REGISTRO (ARCH,I,REG); //SEEK(ARCH,POS); READ(ARCH, REG);
              seek(arch,I);
              read(arch,x);
              clavestr:=x.nombre;
              reg.clave:=claveStr;
              reg.pos_Arch:=i;
             //MUESTRA_REGISTRO(REG);
             agregar(arbolito,reg);

           END;
         END; }

       {     procedure SintCrearSint(var arch:T_ARCHIVO_SINTOMAS);
   begin
     AssignFile(arch,nomArchSintomas);
      if not FileExists(nomArchSintomas) then Rewrite(arch) else reset(arch);

   end;        }
  { procedure SintPosFinalArchivoSint(var arch: T_ARCHIVO_SINTOMAS);
     var
       tam:integer;
     begin
   //    reset(arch);
       tam:=FileSize(arch);
       Seek(arch,tam);
     end; }


// procedure Sint leerArchivo (var arch: T_ARCHIVO_SINTOMAS);
 //procedure SintrecuperarARCHSint(arch:T_ARCHIVO_SINTOMAS;var x:t_dato);


end.

