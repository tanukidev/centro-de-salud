unit menu_opciones;

{$mode objfpc}{$H+}  {$codepage utf-8}

interface
    uses
 crt,tiposdedatos,archivo, unit_arbol,codemonkey,manejo_arbol,ESTADISTICAS,sysutils;

const
  n=20 ;
  m=30 ;
  l=12;
type
    T_string=string;

    T_vector= array[1..n] of T_dato;
    {T_vector_string= array[1..n]of T_string; }


      Procedure menu () ;


implementation



       Procedure menu () ;

       var
       i:integer  ;
       respuesta,dia:integer;
       buscado, position: word;
       enc,control:boolean;
       pos:pos_Arch;

       archP:T_archivo ;
       archS:T_archivo_Sintomas;
       X,REG:T_dato;
       Y:T_SINTOMAS;
       arbolitoP:T_punt;
       arbolitoN:T_punt;

       fechain,fechafin,fecha:t_fecha;

      // vec:T_vector;
       selection,entrada:string;
       aux,aux2, aux3,posx,posy,entero:integer;
       largo_y, contador:integer;
       palabras:string;
         OPCION:opcionC;
       arbol_personas,arbol_sintomas:t_punt;
       //vector_palabras:T_vector_string ;

          procedure agregar_elemento_menu (var y:integer; var y_largo:integer; var contador:integer; texto:string);

                    begin

                        gotoxy(50,y);
                        inc(contador,1);
                        write(contador,')'+texto);
                        y:=y+2;
                        y_largo:=y+4;
                    end;
          procedure escribir (texto:string;var entrada:string; var posx,posy:byte);

                    begin
                        gotoxy(posx,posy);
                        writeln(texto);
                        readln(entrada);
                        inc(posy,1);
                        end;

 begin
       textcolor(lightgreen);

       aux:=10;
       //inicialización
       crear(archp);
       crearS(archS);

       CREAR_ARBOL(arbolitoP);
       CREAR_ARBOL(arbolitoN);
       cargararbolDNI(archP,arbolitoP);
       cargararbolnom(archP,arbolitoN);


  REPEAT









  gotoxy(50,5);
  WRITE ('/////MENU DE OPCIONES/////');
  aux2:=5  ;
  largo_y:=6;
  contador:=0;

  agregar_elemento_menu(aux2, largo_y,contador, ' ALTA/MODIFICACION/CONSULTA DE DATOS') ;
  agregar_elemento_menu(aux2, largo_y,contador, ' LISTADO ORDENADO POR FECHA') ;
  agregar_elemento_menu(aux2, largo_y, contador, 'LISTADO POR FECHAS DE CONSULTA');
  agregar_elemento_menu(aux2, largo_y, contador, 'porcentaje consultas diarias');
  agregar_elemento_menu(aux2, largo_y, contador, 'porcentaje consultas vecinas');
  agregar_elemento_menu(aux2, largo_y, contador, 'promedio de consultas diarias');
  agregar_elemento_menu(aux2, largo_y, contador, 'porcentaje menores de 25');
  contador:=12;
  agregar_elemento_menu(aux2, largo_y, contador, 'eliminar archivo');




  for i:=0 to 15 do   //horizontal - arriba
                 begin
                  inc(aux, 2);
                    gotoxy(aux,3);
                      write('-');
                 end;
        for i:=5 to largo_y do       //vertical izquierda
                   begin
                      gotoxy(48,i)  ;
                      write('|');
                   end;
          aux:=48;
          largo_y:=largo_y+2;
         for i:=0 to 15 do //horizontal - avajo
                 begin
                  inc(aux, 2);
                    gotoxy(aux,largo_y);
                      write('-');
                 end;
        for i:=5 to largo_y do               //vertical derecha
                   begin
                      gotoxy(100,i)  ;
                      write('|');
                   end;
   largo_y:=largo_y-1;
  gotoxy(50,largo_y+2);
  write('Seleccione opcion: ');
  readln(respuesta);



CASE respuesta OF
     1:begin
        WRITELN('PRESIONE 1 PARA ALTA O 2 PARA MODIFICACION/CONSULTA');
        READLN(opcion);
        ABMC_personas(opcion,archP,archS,arbolitoP);


     end;



  2:begin   //mostrar datos de paciente
          clrscr;
          gotoxy(50,1);
          aux:=1;aux2:=1;i:=1  ;
          control:=false ;
          seek(archp,0);
          while not(EOF(archP))DO
           BEGIN
            // LEE_REGISTRO (ARCH,I,REG); //SEEK(ARCH,POS); READ(ARCH, REG);
              seek(archp,i);
              read(archP,x);

             textcolor(lightred);
              writeln('numero de consulta ',x.ID);
              textcolor(lightgreen);
              writeln('Apellido ',x.Apellido);
              writeln('Nombres ',x.Nombre);
              writeln('ciudad ',x.ciudad );
              writeln('direccion ',x.direccion);
              writeln('Número de documento ',x.DNI);
              writeln('Teléfono ',x.TEL );

              seek(archS,I);
              if posExist(archS,y,i) = true then
                                     begin
                                     read(archS,y) ;
                                     writeln(y.FECHAhoy.dia,'/',y.FECHAhoy.mes,'/',y.FECHAhoy.anno);
                                     end
              else writeln('no se encuentra entrada de sintomas para este paciente');
              inc(aux,1);
              if (aux-aux2) = 3 then begin
                                       writeln('1: mostrar mas');writeln('2: salir');
                                       readln(entrada);
                                       aux2:=aux;
                                     end
                                     else if entrada = '2' then control:=true;

              inc(i,1);
            end;

            writeln('presione una tecla para continuar');
         readkey;
          clrscr ;
          end;
        3: begin
        writeln ('ingrese la fecha de la cual desea ver las consultas:');
        enc:=false ;
         readln(dia);
         fecha.dia:=entero;
         write('-');
         readln(entero);
         fecha.mes:=entero;
         write('-');
         readln(entero);
         fecha.anno:=entero;

         pos:=0;
         while (enc=false) and not(eof(archs))do

               begin
             seek(archs,pos);
             read(archs,y);
             inc(pos,1);

            If (fecha.dia=y.FECHAhoy.dia )and( fecha.mes=y.FECHAhoy.mes) and (fecha.anno=y.FECHAhoy.anno) Then
               begin
                  enc:=true ;
                  mostrar_todo_T_dato(x);
               end;
          end;
         clrscr;
        end;
        4:
        begin
               CLRSCR;
               GOTOXY(5,5);
            writeln(' A CONTINUACION SE PRESENTAN LOS CASOS CONFIRMADOS');
        FOR I:= 0 TO FILESIZE(ARCHP)-1 DO
           BEGIN
            // LEE_REGISTRO (ARCH,I,REG); //SEEK(ARCH,POS); READ(ARCH, REG);
              seek(archS,I);
              seek(archP,I);
              textcolor(red);
              if posExist(archS,y,i) = true then read(archS,y) else writeln('no hay entrada de sintomas para esta persona');
              textcolor(lightblue);
              read(archP,x);
              mostrar_todo_T_dato(x);
              writeln(' presione para continuar') ;
              readkey;
              end;//mostrar nombre de los que dieron positivo
        clrscr;
        end;
        5: begin
        clrscr;
        porcentaje_confirmados_covid(archS);
         readkey;
        end;
        6: begin
        clrscr ;
        porcentaje_consultas_diarias(archP);
        end;

        7:begin
        clrscr ;
        porcentaje_consultas_vecinos(archP);
        end;
        8:begin
        clrscr;
        cant_confirmados_covid(archS );
        end;
        9:begin
            clrscr;
            porcentaje_menores25(archP);

        end;
        10: begin
        clrscr;
        gotoxy(20,5)  ;
        writeln('ingrese desde que fecha buscar') ;
        writeln('ingrese dia');
        readln(fechain.dia);
        writeln('ingrese mes');
        readln(fechain.mes);
        writeln('ingrese año');
        readln(fechain.anno);


        consultas_entre_fechas(convertT_fechaAint(fechain),convertT_fechaAint(fechafin),archS);
        end;
        11: begin
        clrscr;
        writeln('mostrar listado ordenado por fecha(1) o por nombre(2)?');
        readln(buscado);
        case buscado of
                 1:  begin
                     writeln('listado ordenado por NOmbre confirmados positivos');
                      inorden(arbolitoP,2,archP,archS);
                      readkey;
                      clrscr;
                 end;
                   2:begin
                   writeln('listado ordenado por nombre');
                   inorden(arbolitoN,1,archP,archS) ;
                   readkey;
                   clrscr;
             end;  end;


        end;
        12: begin
              close(archp);
              close(archS);
              erase(archP);
              erase(archS);
              respuesta:=0;

        end;
        end;
  UNTIL respuesta=0;

  close(archp);
  close(archS);

end;



end.

